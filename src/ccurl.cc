#include "ccurl.h"
#include <curl/curl.h>

size_t write_callback(void *donnee, size_t taille_type, size_t nombre, std::string* enregistreur) {
    enregistreur->append((char*)donnee, taille_type*nombre);

    return taille_type*nombre;
}

CCurl::CCurl(){

}

CCurl::~CCurl(){

}

void CCurl::post(std::string url, std::string conteneur, struct curl_slist * base_header) {
    m_source_entete.clear();
    m_source_html.clear();

    CURL* m_curl = curl_easy_init();

    struct curl_slist *header = nullptr;
    if ( base_header != nullptr) {
        header = base_header;
    }

    header = curl_slist_append(header, "Cookie: allow all");
    header = curl_slist_append(header, "User-Agent: LibUapvENT/1.0");

    if ( m_curl != nullptr){
        curl_easy_setopt(m_curl, CURLOPT_URL, url.data());

        curl_easy_setopt(m_curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_easy_setopt(m_curl, CURLOPT_HTTPHEADER, header);

        curl_easy_setopt(m_curl, CURLOPT_VERBOSE, 1L);

        curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, write_callback);
        curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, &m_source_html);

        curl_easy_setopt(m_curl, CURLOPT_HEADERFUNCTION, write_callback);
        curl_easy_setopt(m_curl, CURLOPT_HEADERDATA, &m_source_entete);

        if ( !conteneur.empty() ) {
            curl_easy_setopt(m_curl, CURLOPT_POSTFIELDSIZE, conteneur.size());
            curl_easy_setopt(m_curl, CURLOPT_POSTFIELDS, (char*)&conteneur[0]);
        }

        int res = curl_easy_perform(m_curl);
    }
    curl_easy_getinfo(m_curl, CURLINFO_RESPONSE_CODE, &m_code_reponse);

    curl_slist_free_all(header);
    curl_easy_cleanup(m_curl);
}

void CCurl::get(std::string url, struct curl_slist * base_header) {
    m_source_entete.clear();
    m_source_html.clear();

    CURL* m_curl = curl_easy_init();

    struct curl_slist *header = nullptr;
    if ( base_header != nullptr) {
        header = base_header;
    }

    header = curl_slist_append(header, "Cookie: allow all");
    header = curl_slist_append(header, "User-Agent: LibUapvENT/1.0");


    if ( m_curl != nullptr){
        curl_easy_setopt(m_curl, CURLOPT_URL, url.data());

        curl_easy_setopt(m_curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_easy_setopt(m_curl, CURLOPT_HTTPHEADER, header);
        curl_easy_setopt(m_curl, CURLOPT_VERBOSE, 1L);
        curl_easy_setopt(m_curl, CURLOPT_HEADERFUNCTION, write_callback);
        curl_easy_setopt(m_curl, CURLOPT_HEADERDATA, &m_source_entete);

        curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, write_callback);
        curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, &m_source_html);

        int res = curl_easy_perform(m_curl);
    }

    curl_easy_getinfo(m_curl, CURLINFO_RESPONSE_CODE, &m_code_reponse);

    curl_slist_free_all(header);
    curl_easy_cleanup(m_curl);
}
