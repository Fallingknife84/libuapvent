#include "cuapvconnexion.h"
#include "ccurl.h"
#include <curl/curl.h>
#include <cstring>

CUAPVConnexion::CUAPVConnexion(){}

CUAPVConnexion *CUAPVConnexion::m_singleton = nullptr;

CUAPVConnexion::~CUAPVConnexion(){}


bool CUAPVConnexion::uapvConnexion(std::string identifiant, std::string mot_de_passe) {
    if (identifiant.empty() || mot_de_passe.empty()) {
        std::cerr << "Veuillez donner des informations corrects" << std::endl;
        return false;
    }

    CCurl requete;
    // On récupère les élements comme execution.
    requete.post(m_Url_Cas, "");

    size_t pos_exec = requete.m_source_html.find("name=\"execution\" value=\"");
    if ( pos_exec == std::string::npos) {
        return false;
    }

    pos_exec += 24;

    std::string exec = requete.m_source_html.substr(pos_exec, requete.m_source_html.size() - pos_exec);
    pos_exec = exec.find("\"");
    exec = exec.substr(0, pos_exec);

    std::string informations = "username=" + identifiant + "&password=" + mot_de_passe +
        "&execution=" + exec + "&_eventId=submit&geolcoation=";

    struct curl_slist *base_entete = nullptr;
    base_entete = curl_slist_append(base_entete, "Content-Type: application/x-www-form-urlencoded");

    // On envoie les informations de connexions
    requete.post(m_Url_Cas, informations, base_entete);

    if ( requete.m_code_reponse != 302 ) {
        return false;
    }


    // On récupère le lien pour confirmer notre connexion au service;
    pos_exec = requete.m_source_entete.find("Location: ");
    if ( pos_exec == std::string::npos) {
        return false;
    }
    pos_exec += 10;

    std::string location = requete.m_source_entete.substr(pos_exec, requete.m_source_entete.size() - pos_exec);
    pos_exec = location.find("cas0") + 6;
    location = location.substr(0, pos_exec);

    // Ici on confirme notre connexion.
    requete.get(location);

    // On récupère notre billet pour avoir le jeton
    pos_exec = location.find("ticket=");
    pos_exec += 7;

    m_billet = location.substr(pos_exec, location.size() - pos_exec);

    return true;
}

std::string CUAPVConnexion::uapvEnvoiTrame(Chercher recherche, std::array<std::string, 4>* params) {
    CCurl requete;

    //Si le jeton est vide alors on se connect à l'api de l'edt
    if ( m_jeton.empty()) {
        const std::string lien_auth = m_Url_Edt + "auth/login";
        const std::string conteneur = "{ \"version\": \"\", \"ticket\": \"" + m_billet  +"\", \"service\": \"https://edt.univ-avignon.fr\" }";

        struct curl_slist *base_entete = nullptr;

        //std::cout << conteneur << std::endl;

        // Ces deux valeurs sont obligatoire pour que le serveur nous autorise.
        base_entete = curl_slist_append(base_entete, "Content-Type: application/json");
        base_entete = curl_slist_append(base_entete, "Origin: https://edt.univ-avignon.fr");

        requete.post(lien_auth, conteneur, base_entete);

        //Récupérer uniquement le token sans à devoir à parser le contenue.
        size_t pos_exec = requete.m_source_html.find("token\":\"");
        if ( pos_exec == std::string::npos) {
            return "{}";
        }
        pos_exec += 8;

        m_jeton = requete.m_source_html.substr(pos_exec, requete.m_source_html.size() - pos_exec);
        pos_exec = m_jeton.find("\"");
        m_jeton = m_jeton.substr(0, pos_exec);
    }

    std::string lien = m_Url_Edt;
    // On configure le lien pour avoir les informations en JSON
    if( recherche == Chercher::EDT_FAVORIS ) {
        lien += "events_perso?autre=false";
    } else {
        if ( params == nullptr || params->size() != 4) {
            return "{}";
        }

        lien += "salles/disponibilite?site=" + params->at(0);
        lien += "&duree="  + params->at(1);
        lien += "&debut=" + params->at(2);
        lien += "&date=" + params->at(3);
    }

    struct curl_slist *base_entete = nullptr;

    // L'origine et le token sont indispensable.
    const std::string jeton_entete = "token: " + m_jeton;
    base_entete = curl_slist_append(base_entete, (char*)&jeton_entete[0]);

    base_entete = curl_slist_append(base_entete, "Origin: https://edt.univ-avignon.fr");

    requete.get(lien, base_entete);

    // On a reçu le json.
    std::string retour_trame = requete.m_source_html;

    return retour_trame;
}

CUAPVConnexion *CUAPVConnexion::recupInstance(){
    if(m_singleton == nullptr){
        std::cout << "Création de l'objet de connexion" << std::endl;
        m_singleton = new CUAPVConnexion();
    }else{
        std::cout << "Il y a déjà un objet de connexion existant." << std::endl;
    }
    return(m_singleton);
}

void CUAPVConnexion::libererInstance(){
    if(m_singleton == nullptr){
        return;
    }
    delete(m_singleton);
}
